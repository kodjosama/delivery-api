<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeliveryObject extends Model
{
    use HasFactory;

    protected $fillable = [
        'type',
        'length',
        'width',
        'height',
        'weight',
        'comments',
        'delivery_id',
    ];

    protected $casts = [
        'type' => 'json',
    ];

    public function delivery()
    {
        return $this->belongsTo(Delivery::class);
    }
}
