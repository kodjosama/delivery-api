<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    use HasFactory;

    protected $fillable = [
        'support_address',
        'deposit_address',
        'recipient',
        'express',
        'quantity',
        'insurance',
    ];

    protected $casts = [
        'supported' => 'boolean',
        'insurance' => 'boolean',
        'express' => 'boolean',
    ];

    public function objects()
    {
        return $this->hasMany(DeliveryObject::class);
    }
}
