<?php

namespace App\Http\Controllers;

use App\Models\Delivery;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DeliveryController extends Controller
{
    public function index()
    {
        return Delivery::with('objects')->get();
    }

    public function store(Request $request)
    {
        $validated = $this->validate($request, [
            'support_address' => 'required',
            'deposit_address' => 'required',
            'recipient' => 'required',
            'supported' => 'required',
            'express' => 'required',
            'quantity' => 'required',
            'insurance' => 'required',
            'objects' => 'required|array|min:1',
            'objects.*.type' => 'required|array',
            'objects.*.type.id' => 'required',
            'objects.*.type.text' => 'required',
            'objects.*.length' => 'required',
            'objects.*.width' => 'required',
            'objects.*.height' => 'required',
            'objects.*.weight' => 'required',
            'objects.*.comments' => 'required',
        ]);

        $delivery = Delivery::create($validated);

        $delivery->objects()->createMany($validated['objects']);

        return new JsonResponse('', 201);
    }
}
