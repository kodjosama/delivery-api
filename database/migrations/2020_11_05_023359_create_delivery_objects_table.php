<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryObjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_objects', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->float('length');
            $table->float('width');
            $table->float('height');
            $table->float('weight');
            $table->string('comments');
            $table->foreignId('delivery_id')->nullable()->constrained('deliveries');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_objects');
    }
}
